import os

font_path = os.path.join(os.path.dirname(__file__).replace('src/edibles_game', ''),
                         "data/edibles_game/fonts/Condition.ttf")
snake_song_path = os.path.join(os.path.dirname(__file__).replace('src/edibles_game', ''),
                               "data/edibles_game/music/snakesong.wav")
ed_image_path = os.path.join(os.path.dirname(__file__).replace('src/edibles_game', ''),
                             "data/edibles_game/images/ed.png")